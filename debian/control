Source: navit
Section: misc
Priority: optional
Maintainer: Gilles Filippini <pini@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 12),
               fonts-liberation,
               freeglut3-dev,
               imagemagick,
               libdbus-1-dev,
               libdbus-glib-1-dev,
               libfontconfig1-dev,
               libfreetype6-dev,
               libfribidi-dev,
               libgarmin-dev,
               libglc-dev (>= 0.7.1),
               libgps-dev (>= 3.1),
               libgtk2.0-dev,
               libimlib2-dev,
               libpq-dev,
               libprotobuf-c-dev,
               librsvg2-bin,
               libsdl-image1.2-dev,
               libspeechd-dev,
               libxml2-dev,
               protobuf-c-compiler
Standards-Version: 4.5.0
Homepage: https://www.navit-project.org
Vcs-Browser: https://salsa.debian.org/debian/navit
Vcs-Git: https://salsa.debian.org/debian/navit.git
Rules-Requires-Root: no

Package: navit
Architecture: any
Depends: navit-data (= ${source:Version}),
         navit-gui-internal (= ${binary:Version}) | navit-gui-gtk (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: gpsd
Suggests: maptool
Description: Car navigation system with routing engine
 Navit is a car navigation system with routing engine.
 .
 Its modular design is capable of using vector maps of various formats for
 routing and rendering of the displayed map. It's even possible to use multiple
 maps at a time.
 .
 The GTK+ or SDL user interfaces are designed to work well with touch screen
 displays. Points of Interest of various formats are displayed on the map.
 .
 The current vehicle position is either read from gpsd or directly from NMEA
 GPS sensors.
 .
 The routing engine not only calculates an optimal route to your destination,
 but also generates directions and even speaks to you using speech-dispatcher.
 .
 The Debian packaging for navit doesn't provide any map. To see, where you can
 get maps from, see <http://wiki.navit-project.org/index.php/Main_Page#Maps>.

Package: navit-gui-gtk
Architecture: any
Depends: navit-graphics-gtk-drawing-area (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: navit (= ${binary:Version})
Description: Car navigation system with routing engine - GTK+ GUI
 Navit is a car navigation system with routing engine.
 .
 Its modular design is capable of using vector maps of various formats for
 routing and rendering of the displayed map. It's even possible to use multiple
 maps at a time.
 .
 The GTK+ or SDL user interfaces are designed to work well with touch screen
 displays. Points of Interest of various formats are displayed on the map.
 .
 The current vehicle position is either read from gpsd or directly from NMEA
 GPS sensors.
 .
 The routing engine not only calculates an optimal route to your destination,
 but also generates directions and even speaks to you using speech-dispatcher.
 .
 This package contains the GTK+ GUI plugin.

Package: navit-gui-internal
Architecture: any
Depends: navit-graphics-gtk-drawing-area (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: navit (= ${binary:Version})
Description: Car navigation system with routing engine - internal GUI
 Navit is a car navigation system with routing engine.
 .
 Its modular design is capable of using vector maps of various formats for
 routing and rendering of the displayed map. It's even possible to use multiple
 maps at a time.
 .
 The GTK+ or SDL user interfaces are designed to work well with touch screen
 displays. Points of Interest of various formats are displayed on the map.
 .
 The current vehicle position is either read from gpsd or directly from NMEA
 GPS sensors.
 .
 The routing engine not only calculates an optimal route to your destination,
 but also generates directions and even speaks to you using speech-dispatcher.
 .
 This package contains the internal GUI plugin.

Package: navit-graphics-gtk-drawing-area
Architecture: any
Depends: librsvg2-common, ${misc:Depends}, ${shlibs:Depends}
Recommends: navit (= ${binary:Version})
Description: Car navigation system with routing engine - GTK+ graphic plugin
 Navit is a car navigation system with routing engine.
 .
 Its modular design is capable of using vector maps of various formats for
 routing and rendering of the displayed map. It's even possible to use multiple
 maps at a time.
 .
 The GTK+ or SDL user interfaces are designed to work well with touch screen
 displays. Points of Interest of various formats are displayed on the map.
 .
 The current vehicle position is either read from gpsd or directly from NMEA
 GPS sensors.
 .
 The routing engine not only calculates an optimal route to your destination,
 but also generates directions and even speaks to you using speech-dispatcher.
 .
 This package contains the graphic gtk-drawing-area plugin to use with GTK+
 aware GUIs plugins.

Package: navit-data
Architecture: all
Depends: ${misc:Depends}
Recommends: navit
Description: Car navigation system with routing engine - data files
 Navit is a car navigation system with routing engine.
 .
 Its modular design is capable of using vector maps of various formats for
 routing and rendering of the displayed map. It's even possible to use multiple
 maps at a time.
 .
 The GTK+ or SDL user interfaces are designed to work well with touch screen
 displays. Points of Interest of various formats are displayed on the map.
 .
 The current vehicle position is either read from gpsd or directly from NMEA
 GPS sensors.
 .
 The routing engine not only calculates an optimal route to your destination,
 but also generates directions and even speaks to you using speech-dispatcher.
 .
 This package contains the architecture independent files - mainly translation
 files and images - for navit.

Package: maptool
Architecture: any
Provides: osm2navit
Replaces: osm2navit
Conflicts: osm2navit
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: navit
Description: Converts OpenStreetMap maps to Navit
 Navit is a car navigation system with routing engine.
 .
 Its modular design is capable of using vector maps of various formats for
 routing and rendering of the displayed map. It's even possible to use multiple
 maps at a time.
 .
 This package contains maptool, a tool to convert OpenStreetMap maps to the
 Navit format.
 .
 Note that maptool is only built on 64bit architectures.
